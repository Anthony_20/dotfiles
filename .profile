#!/bin/sh

#alias
export PATH="$PATH:$HOME/.local/bin"
export SUDO_ASKPASS="$HOME/.local/bin/dmenuPass"

#default brightness
brightnessctl -q s 15%

#Default Shortcuts
export BROWSER="firefox"

#startx
startx
